import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {LoginPage,PropertyListPage,BrokerListPage,FavoriteListPage,WelcomePage,AboutPage,SignupPage} from '../pages/pages';
import { AuthServiceProvider } from "../providers/auth-service/auth-service";
import firebase from 'firebase';

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = WelcomePage;
    userInfo:any;
    appMenuItems: Array<MenuItem>;

    accountMenuItems: Array<MenuItem>;

    helpMenuItems: Array<MenuItem>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private authService: AuthServiceProvider) {    
        
         this.initializeApp();
       
         firebase.auth().onAuthStateChanged((user) => {
                if (!user) {
                    console.log("not login");
                    this.rootPage = LoginPage;

                } 
                // else {
                //     console.log("login");
                //     this.userInfo=firebase.auth().currentUser;
                //     this.rootPage = WelcomePage;
                // }

            });

       
        this.appMenuItems = [
            {title: 'Properties', component: PropertyListPage, icon: 'home'},
            {title: 'Brokers', component: BrokerListPage, icon: 'people'},
            {title: 'Favorites', component: FavoriteListPage, icon: 'star'},
            //{title: 'Get Preapproved', component: WelcomePage, icon: 'checkmark-circle'},
        ];

        this.accountMenuItems = [
            // {title: 'My Account', component: SignupPage, icon: 'ios-contact'},
            {title: 'Logout', component: LoginPage, icon: 'log-out'},
        ];

        this.helpMenuItems = [
            {title: 'Welcome', component: WelcomePage, icon: 'bookmark'},
            {title: 'About', component: AboutPage, icon: 'information-circle'},
        ];
               

        
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleLightContent();
            this.splashScreen.hide();
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    logout() {
    this.authService.logout();
  }
}
