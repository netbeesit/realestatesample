import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';


import {LoginPage,PropertyListPage,BrokerListPage,FavoriteListPage,WelcomePage,AboutPage,SignupPage,PropertyDetailPage,BrokerDetailPage} from '../pages/pages';

import {PropertyService} from "../providers/property-service";
import {BrokerService} from "../providers/broker-service";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

//firebase api
import {FireBaseAPI} from '../shared/shared.api';
import { AuthServiceProvider } from "../providers/auth-service/auth-service";

//firebase configuration
export const firebaseConfig= {
    apiKey: "AIzaSyBlCWrRhoXdoCHjqLVXubtR9sOg_F1W2xc",
    authDomain: "realestate-d9723.firebaseapp.com",
    databaseURL: "https://realestate-d9723.firebaseio.com",
    projectId: "realestate-d9723",
    storageBucket: "realestate-d9723.appspot.com",
    messagingSenderId: "741592655086"
  };

@NgModule({
  declarations: [
    MyApp,
    WelcomePage,
    AboutPage,
    PropertyListPage,
    PropertyDetailPage,
    FavoriteListPage,
    BrokerListPage,
    BrokerDetailPage,
    LoginPage,
     SignupPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    AboutPage,
    PropertyListPage,
    PropertyDetailPage,
    FavoriteListPage,
    BrokerListPage,
    BrokerDetailPage,
      LoginPage,
     SignupPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    PropertyService,
    BrokerService,
     AuthServiceProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
