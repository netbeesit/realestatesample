import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Injectable()
export class FireBaseAPI{
  
  contact = {
  userid: '',
  fullname: '',
  mobileNo: '',
  email: '',
  photoUrl: ''
};

// vehicle = {
//   id:'',
//   userid: '',
//   make: '',
//   model: '',
//   regNo: '',
//   revenueRenewalDate: '',
//   IsuranceRenewalDate: '',
//   sendAlert: true,
 
// };

 driver = {
  id: '',
  vehicleId:'',
  fullname: '',
  mobileNo: '',
  licenseExpDate: '',
  sendAlert: true,
};

// static get parameters() {
  //     return [[Http]];
  // }

  vehicles: FirebaseListObservable<any>;

  //private baseVehicleUrl='http://itrackmyvehicle.com/api/sensor/';
  //private baseUrl='https://vehicletracker-d7532.firebaseio.com/4';
  //private baseUrl='https://restcountries.eu/rest/v2/all';
  
  constructor(public afd: AngularFireDatabase) {

            this.vehicles=this.afd.list('/vehicles/');
   }
 
  getVehicle(customerId) {

    return this.afd.list('/vehicles/',{
      query: {
              orderByChild: 'customerId',
              indexOn: "customerId",
              equalTo: customerId
          }
    });

  }
 
  addVehicle(id,userId,make,made,regNo,revenueRenewalDate,insuranceRenewalDate,sendAlert) {

    return  this.vehicles.push({
          customerId: userId,
          make: make,
          model: made,
          regNo: regNo,
          revenueRenewalDate: revenueRenewalDate,
          IsuranceRenewalDate: insuranceRenewalDate,
          sendAlert: sendAlert,
      });

  }
 
  removeItem(id) {
    this.afd.list('/vehicles/').remove(id);
  }
  

 private extractData(res: Response) {
        let body = res.json();
        return body || { };
    }

 private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}