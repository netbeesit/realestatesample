import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AngularFireDatabase ,FirebaseListObservable} from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';

@Injectable()
export class TrackerAPI{

    static get parameters() {
        return [[Http]];
    }

    items: FirebaseListObservable<any[]>; 
    vehiclelist: FirebaseListObservable<any>;
 
    //private baseVehicleUrl='http://itrackmyvehicle.com/api/sensor/';
    private baseUrl='https://vehicletracker-d7532.firebaseio.com/4';
    // private baseUrl='https://restcountries.eu/rest/v2/all';
   // private baseUrl='http://localhost:6634/api/customer/getdata/1';
    
    constructor(private http:Http, private afDB: AngularFireDatabase){

        //this.items = afDB.list('/vehicles');
    }

  

 getFireBasedata():  FirebaseListObservable<any[]> {
        return this.afDB.list('/4');
    }

    // getVehicles(): Observable<any[]> {
    //     return this.http.get(this.baseUrl)
    //                 .map(this.extractData)
    //                 .catch(this.handleError);
    // }

    // getCountries(): Observable<string[]> {
    //     return this.http.get(this.baseUrl)
    //                 .map(this.extractData)
    //                 .catch(this.handleError);
    // }


    private extractData(res: Response) {
        let body = res.json();
        return body || { };
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}