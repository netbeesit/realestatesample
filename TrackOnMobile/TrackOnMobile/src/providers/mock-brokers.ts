let brokers: Array<any> = [
    {
        id: 1,
        name: "Kanjana Weerakodi",
        title: "Broker",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
        email: "kanjana@casentra.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/caroline_kingsley.jpg"
    },
    {
        id: 2,
        name: "Jonathan Trod",
        title: "Senior Broker",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
     
         email: "jonathan@casentra.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"
    },
    {
        id: 3,
        name: "Navaseelan N",
        title: "Helper",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
           email: "navaseelan4u@gmail.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/jonathan_bradley.jpg"
    },
    {
        id: 4,
        name: "Anna David",
        title: "Asst. Broker",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
        email: "anna@casentra.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/jennifer_wu.jpg"
    },
    {
        id: 5,
        name: "Olivar Twist",
        title: "Broker",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
        email: "olivar@casentra.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/olivia_green.jpg"
    },
    {
        id: 6,
        name: "Abi Muhamad",
        title: "Broker",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
        email: "abiM@casentra.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/miriam_aupont.jpg"
    },
    {
        id: 7,
        name: "John Lambert",
        title: "Senior Broker",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
        email: "lambert@casentra.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michelle_lambert.jpg"
    },
    {
        id: 8,
        name: "Victor Metha",
        title: "Senior Broker",
        phone: "+94-774 -475196",
        mobilePhone: "+94-774 -475196",
        email: "metha@casentra.com",
        picture: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/victor_ochoa.jpg"
    }
];

export default brokers;
