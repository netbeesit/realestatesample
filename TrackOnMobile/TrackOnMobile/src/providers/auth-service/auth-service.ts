import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthServiceProvider {
 
  user: Observable<firebase.User>;
  public fireAuth: any;
  public userData: any;

  constructor(private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;
  }
  
  login(email: string, password: string): any {
      return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }

  signup(email: string, password: string): any {
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
  }
  
  resetPassword(email: string): any {
  return this.firebaseAuth.auth.sendPasswordResetEmail(email);
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }

}
