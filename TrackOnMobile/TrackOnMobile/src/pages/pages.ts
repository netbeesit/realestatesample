export * from "./welcome/welcome";
export * from './login/login.page';
export * from './about/about';
export * from './broker-detail/broker-detail';
export * from './broker-list/broker-list';
export * from './favorite-list/favorite-list';
export * from './signup/signup.page';
export * from './property-list/property-list';
export * from './property-detail/property-detail';



