import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, LoadingController  } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';

import { WelcomePage,SignupPage } from '../../pages/pages';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";


@Component({
  selector: 'page-login',
  templateUrl: 'login.page.html'
})
export class LoginPage {

  public loginForm;
  emailChanged: boolean = false;
  passwordChanged: boolean = false;
  submitAttempt: boolean = false;
  loading: any;

  constructor(private navCtrl: NavController,private authService: AuthServiceProvider,
  public navParams: NavParams, public formBuilder: FormBuilder,public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    
    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEXP)])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
     
  }


  elementChanged(input){
    let field = input.inputControl.name;
    this[field + "Changed"] = true;
  }
 
  login() {

      this.submitAttempt = true;
      if (!this.loginForm.valid){
      console.log(this.loginForm.value);
       } 
    else 
    {
      // enable the loader [busy]
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();

      
      //pass the data to auth service
      this.authService.login(this.loginForm.value.email,this.loginForm.value.password)
      .then(value => 
      {
        this.navCtrl.setRoot(WelcomePage);
      }, 
      error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      
   }

  }

  logout() {
    this.authService.logout();
  }

   register(){
    this.navCtrl.push(SignupPage);
  }

  reset(){
    this.navCtrl.push(SignupPage);
  }
}
