import { OnInit,Component } from '@angular/core';
import { NavController, AlertController, NavParams, LoadingController  } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { LoginPage,WelcomePage } from '../../pages/pages';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import firebase from 'firebase';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.page.html'
})

export class SignupPage {

  signupForm: FormGroup;
  userInfo: {email: string, password: string,  fullname: string,mobileNo:string} = 
  {email: '', password: '', fullname: '', mobileNo: ''};


  emailChanged: boolean = false;
  passwordChanged: boolean = false;
  fullnameChanged: boolean = false;
  submitAttempt: boolean = false;
  loading: any;
  public userData: any;

  constructor(private navCtrl: NavController,private authService: AuthServiceProvider,
  public navParams: NavParams, public formBuilder: FormBuilder,public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    
    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.signupForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEXP)])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      fullname: ['', [Validators.required, Validators.minLength(4), this.nameValidator.bind(this)]],
      mobileNo: ['', [Validators.required,this.phoneValidator.bind(this)]]
    });
      
  }

  // ngOnInit(): any {
  //   this.signupForm = this.formBuilder.group({
  //     'password':  ['', [Validators.required, Validators.minLength(6)]],
  //     'fullname': ['', [Validators.required, Validators.minLength(4), this.nameValidator.bind(this)]],
  //     'email': ['', [Validators.required,this.emailValidator.bind(this)]],
  //     'mobileNo': ['', [Validators.required,this.phoneValidator.bind(this)]]
  //   });
  // }

 
 isValid(field: string) {
    
    return true;
  }

  nameValidator(control: FormControl): {[s: string]: boolean} {
    if (!control.value.match("^[a-zA-Z ,.'-]+$")) {
      return {invalidName: true};
    }
  }

  phoneValidator(control: FormControl): {[s: string]: boolean} {
    if (control.value !== '') {
      if (!control.value.match('\\(?\\d{3}\\)?-? *\\d{3}-? *-?\\d{4}')) {
        return {invalidPhone: true};
      }
    }
  }
 

  elementChanged(input){
    let field = input.inputControl.name;
    this[field + "Changed"] = true;
  }

  signup() {
    
        this.submitAttempt = true;

        this.loading = this.loadingCtrl.create({
            dismissOnPageChange: true,
        });
        this.loading.present();


        if (!this.signupForm.valid){
            console.log(this.signupForm.value);
        } else {

        this.authService.signup(this.signupForm.value.email, this.signupForm.value.password)
        .then((newUser) => {
            firebase.database().ref('/customers').child(newUser.uid).set({
            fullName: this.signupForm.value.fullname,
            email: this.signupForm.value.email,
            mobileNo: this.signupForm.value.mobileNo
          });
          
            this.navCtrl.setRoot(WelcomePage);
        })
        , error => {
            this.loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
                message: error.message,
                buttons: [
                {
                    text: "Ok",
                    role: 'cancel'
                }
                ]
            });
            alert.present();
            });
        }
    }
}

login(){
    this.navCtrl.setRoot(LoginPage);
  }

}
